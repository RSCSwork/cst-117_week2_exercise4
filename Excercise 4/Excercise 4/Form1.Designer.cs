﻿namespace Excercise_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.secondsElapsedLbl = new System.Windows.Forms.Label();
            this.displayLbl = new System.Windows.Forms.Label();
            this.convertBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.secondsEnteredTxtBx = new System.Windows.Forms.TextBox();
            this.displayTxtBx = new System.Windows.Forms.TextBox();
            this.messageLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // secondsElapsedLbl
            // 
            this.secondsElapsedLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.secondsElapsedLbl.AutoSize = true;
            this.secondsElapsedLbl.Location = new System.Drawing.Point(23, 99);
            this.secondsElapsedLbl.MinimumSize = new System.Drawing.Size(150, 25);
            this.secondsElapsedLbl.Name = "secondsElapsedLbl";
            this.secondsElapsedLbl.Size = new System.Drawing.Size(279, 25);
            this.secondsElapsedLbl.TabIndex = 0;
            this.secondsElapsedLbl.Text = "Enter the number of seconds elapsed:";
            // 
            // displayLbl
            // 
            this.displayLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.displayLbl.AutoSize = true;
            this.displayLbl.Location = new System.Drawing.Point(33, 158);
            this.displayLbl.MinimumSize = new System.Drawing.Size(150, 25);
            this.displayLbl.Name = "displayLbl";
            this.displayLbl.Size = new System.Drawing.Size(153, 25);
            this.displayLbl.TabIndex = 1;
            this.displayLbl.Text = "Seconds Converted:";
            // 
            // convertBtn
            // 
            this.convertBtn.Location = new System.Drawing.Point(138, 223);
            this.convertBtn.MinimumSize = new System.Drawing.Size(100, 40);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(100, 40);
            this.convertBtn.TabIndex = 3;
            this.convertBtn.Text = "Convert";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.ConvertBtn_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(310, 223);
            this.clearBtn.MinimumSize = new System.Drawing.Size(100, 40);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(100, 40);
            this.clearBtn.TabIndex = 4;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // secondsEnteredTxtBx
            // 
            this.secondsEnteredTxtBx.Location = new System.Drawing.Point(372, 96);
            this.secondsEnteredTxtBx.Name = "secondsEnteredTxtBx";
            this.secondsEnteredTxtBx.Size = new System.Drawing.Size(100, 26);
            this.secondsEnteredTxtBx.TabIndex = 5;
            // 
            // displayTxtBx
            // 
            this.displayTxtBx.Location = new System.Drawing.Point(372, 155);
            this.displayTxtBx.Name = "displayTxtBx";
            this.displayTxtBx.Size = new System.Drawing.Size(100, 26);
            this.displayTxtBx.TabIndex = 6;
            // 
            // messageLbl
            // 
            this.messageLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageLbl.AutoSize = true;
            this.messageLbl.Location = new System.Drawing.Point(72, 40);
            this.messageLbl.MinimumSize = new System.Drawing.Size(400, 20);
            this.messageLbl.Name = "messageLbl";
            this.messageLbl.Size = new System.Drawing.Size(400, 20);
            this.messageLbl.TabIndex = 7;
            this.messageLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 320);
            this.Controls.Add(this.messageLbl);
            this.Controls.Add(this.displayTxtBx);
            this.Controls.Add(this.secondsEnteredTxtBx);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.convertBtn);
            this.Controls.Add(this.displayLbl);
            this.Controls.Add(this.secondsElapsedLbl);
            this.Name = "Form1";
            this.Text = "Seconds Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label secondsElapsedLbl;
        private System.Windows.Forms.Label displayLbl;
        private System.Windows.Forms.Button convertBtn;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.TextBox secondsEnteredTxtBx;
        private System.Windows.Forms.TextBox displayTxtBx;
        private System.Windows.Forms.Label messageLbl;
    }
}

