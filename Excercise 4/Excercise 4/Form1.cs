﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Excercise_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ConvertBtn_Click(object sender, EventArgs e)
        {
            double seconds, minutes, hours, days;
            //seconds = double.Parse(secondsEnteredTxtBx.Text);
            if (double.TryParse(secondsEnteredTxtBx.Text, out seconds))
            {
                if (seconds < 60)
                {
                    displayLbl.Text = "You entered this many seconds:";
                    String secondsText = String.Format("{0:0.000}", seconds);
                    displayTxtBx.Text = Convert.ToString(secondsText);
                }
                else if (seconds >= 60 && seconds < 3600)
                {
                    minutes = (seconds / 60);
                    displayLbl.Text = "Number of Minutes in that many seconds:";
                    String minutesText = String.Format("{0:0.000}", minutes);
                    displayTxtBx.Text = Convert.ToString(minutesText);
                }
                else if (seconds >= 3600 && seconds < 86400)
                {
                    hours = (seconds / 3600);
                    displayLbl.Text = "Number of Hours in that many seconds:";
                    String hoursText = String.Format("{0:0.000}", hours);
                    displayTxtBx.Text = Convert.ToString(hoursText);
                }
                else if (seconds >= 86400)
                {
                    days = (seconds / 86400);
                    displayLbl.Text = "Number of days in that many seconds:";
                    String daysText = String.Format("{0:0.000}", days);
                    displayTxtBx.Text = Convert.ToString(daysText);
                }//ends else if tree for seconds conversion
            }else {
                messageLbl.Text = "TryParse failed, Please re-enter with a value";
            }//ends try parse if else decision. 

        }//ends convert btn

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            secondsEnteredTxtBx.Text = "";
            displayLbl.Text = "Seconds Converted:";
            displayTxtBx.Text = "";
            messageLbl.Text = "";
        }//ends clear btn 

    }//ends class Form1
}//ends namepsace
